<?php

use App\Models\Score;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//CRUD

//CREATE
$router->post('/scores', function (Request $request) use ($router) {

    //var_dump($request);
    $new_score = new Score();
    $new_score->name = $request->input('name');
    $new_score->points = $request->input('points');

    if ($new_score->save()) {
        return response()->json($new_score);
    } else {
        return response()->json("Error al salvar");
    } 
});

//READ
$router->get('/scores', function () use ($router) {
    // return response()->json("ruta /scores");
    return response()->json(Score::all());
});

//UPDATE
$router->patch('/scores', function (Request $request) use ($router) {

    $findObject = Score::where("name", "=", $request->input('name'))->first();
    if ($findObject != null) {
        $findObject->points = $request->input('points');
        $findObject->save();

        return response()->json($findObject);
    }else {
        return response()->json("name not found");
    }
    
});

//DELETE
$router->delete('/scores', function (Request $request) use ($router) {

    $findObject = Score::where("name", "=", $request->input('name'))->first();
    if ($findObject != null) {
        $findObject->delete();      
        return response()->json('name deleted');
    }else {
        return response()->json("name not found");
    }
    
});