<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 *  @property string name
 *  @property int points
 */

class Score extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
}